package com.paulmark.ratingcalculator;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity implements OnSeekBarChangeListener,
		OnKeyListener, OnFocusChangeListener {
	int currentScoreInInt;
	int maxScore = 100;
	EditText et;
	SeekBar sb;
	ArrayAdapter<String> rating;
	RadioButton base50;
	RadioButton base100;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		rating = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1);

		base50 = (RadioButton) findViewById(R.id.rbBase50);
		base50.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				updateRating();
			}
		});

		base100 = (RadioButton) findViewById(R.id.rbBase100);
		base100.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				updateRating();
			}
		});

		et = (EditText) findViewById(R.id.etTotalScore);
		et.setOnKeyListener(this);
		et.setOnFocusChangeListener(this);

		sb = (SeekBar) findViewById(R.id.sbTotalScore);
		sb.setOnSeekBarChangeListener(this);
		sb.setMax(maxScore + 1);
		sb.setProgress(maxScore / 2);

		ListView lv = (ListView) findViewById(R.id.lvRating);
		lv.setAdapter(rating);

		updateSeekBar();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android
	 * .widget.SeekBar, int, boolean)
	 */
	public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
		currentScoreInInt = progress;
		et.setText(progress + "");
		updateRating();
		cusorAtLast();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android
	 * .widget.SeekBar)
	 */
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android
	 * .widget.SeekBar)
	 */
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnKeyListener#onKey(android.view.View, int,
	 * android.view.KeyEvent)
	 */
	public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
		updateSeekBar();
		return false;
	}

	/**
	 * 
	 */
	public void updateSeekBar() {
		String currentScoreInString = et.getText().toString();

		// Checking for too low score
		if (currentScoreInString == null || currentScoreInString.equals("")) {
			currentScoreInString = "0";
		}

		currentScoreInInt = Integer.parseInt(currentScoreInString);

		// Checking for too high score
		if (currentScoreInInt > maxScore) {
			et.setText("200");
			currentScoreInInt = maxScore;
		}

		sb.setProgress(currentScoreInInt);
		updateRating();
		cusorAtLast();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.view.View.OnFocusChangeListener#onFocusChange(android.view.View,
	 * boolean)
	 */
	public void onFocusChange(View arg0, boolean arg1) {
		cusorAtLast();
	}

	/**
	 * 
	 */
	/**
	 * 
	 */
	public void cusorAtLast() {
		et.setSelection(et.getText().length());
	}

	/**
	 * 
	 */
	public void updateRating() {
		rating.clear();
		if (base50.isChecked()) {
			for (int i = currentScoreInInt; i >= 0; i--) {
				int ratingPercentage = (int) (((double) i / (double) currentScoreInInt) * (double) 50) + 50;
				rating.add(i + "pt(s). = " + ratingPercentage + "%");
			}
		} else {
			for (int i = currentScoreInInt; i >= 0; i--) {
				int ratingPercentage = (int) (((double) i / (double) currentScoreInInt) * (double) 100);
				rating.add(i + "pt(s). = " + ratingPercentage + "%");
			}
		}
	}
}
